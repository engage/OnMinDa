package com.newthread.android.bean.remoteBean.bbs;

import android.content.Context;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.client.CommentImage_1Client;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/6/20.
 */
public class CommentImage_1 extends RemoteObject {
    private String imageUrl;
    private String imageSize;
    private Comment comment;//所评论的图片，这里体现的是一对多的关系，一个图片只能属于一个评论

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new CommentImage_1Client(context);
    }
}
