package com.newthread.android.bean.remoteBean.bbs;

import android.content.Context;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.client.CommentClient;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/5/29.
 */
public class Comment extends RemoteObject {

    private String content;//评论内容

    private StudentUser author;//评论的用户，Pointer类型，一对一关系

    private Post post; //所评论的帖子，这里体现的是一对多的关系，一个评论只能属于一个微博

    private Comment comment;//对评论进行评论

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public StudentUser getAuthor() {
        return author;
    }

    public void setAuthor(StudentUser author) {
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id" + getObjectId() +
                "content='" + content + '\'' +
                ", authorIsNull?" + (author == null) +
                ", postIsNull?" + (post == null) +
                ", commentIsNull?" + (comment == null) +
                '}';
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new CommentClient(context);
    }
}
