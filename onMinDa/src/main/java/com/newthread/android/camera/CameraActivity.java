package com.newthread.android.camera; /**
 * 用来拍照的Activity
 * @author Chenww
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;

import com.newthread.android.R;
import com.newthread.android.activity.main.MyApplication;

/**
 * 该activity负责拍照并保持图片
 */
@SuppressLint("NewApi")
public class CameraActivity extends Activity {
    private CallBack callBack;
    private SurfaceHolder myHolder;
    private Camera myCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 无title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 全屏
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 设置布局
        setContentView(R.layout.activity_camera);
        callBack = MyApplication.getInstance().getThing("CameraActivity_callBack");
        // 初始化surface
        initSurface();
        // 这里得开线程进行拍照，因为Activity还未完全显示的时候，是无法进行拍照的，SurfaceView必须先显示
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 初始化camera并对焦拍照
                initCamera();
            }
        }).start();
    }

    // 初始化surface
    @SuppressWarnings("deprecation")
    private void initSurface() {
        // 初始化surfaceview
        SurfaceView mySurfaceView = (SurfaceView) findViewById(R.id.camera_surfaceview);

        // 初始化surfaceholder
        myHolder = mySurfaceView.getHolder();
        myHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    // 初始化摄像头
    private void initCamera() {

        // 如果存在摄像头
        if (checkCameraHardware(getApplicationContext())) {
            // 获取摄像头（首选前置，无前置选后置）
            if (openFacingFrontCamera()) {
                // 进行对焦
                autoFocus();
            }

        }
    }

    // 对焦并拍照
    private void autoFocus() {
        // 自动对焦
        try {
            // 因为开启摄像头需要时间，这里让线程睡两秒
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            if (callBack != null)
                callBack.callResult("thread sleep fail");
        }
        myCamera.autoFocus(myAutoFocus);
        try {
            myCamera.takePicture(null, null, myPicCallback);
        } catch (Exception e) {
            if (callBack != null)
                callBack.callResult("take picture fail");
        }
    }

    // 判断是否存在摄像头
    private boolean checkCameraHardware(Context context) {

        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // 设备存在摄像头
            return true;
        } else {
            // 设备不存在摄像头
            if (callBack != null)
                callBack.callResult("camera not exit");
            return false;
        }

    }

    // 得到后置摄像头
    private boolean openFacingFrontCamera() {
        // 尝试开启前置摄像头
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int camIdx = 0, cameraCount = Camera.getNumberOfCameras(); camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    myCamera = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    if (callBack != null)
                        callBack.callResult("open fail");
                    return false;
                }
            }
        }
        // 如果开启前置失败（无前置）则开启后置
        if (myCamera == null) {
            for (int camIdx = 0, cameraCount = Camera.getNumberOfCameras(); camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    try {
                        myCamera = Camera.open(camIdx);
                    } catch (RuntimeException e) {
                        if (callBack != null)
                            callBack.callResult("open fail");
                        return false;
                    }
                }
            }
        }
        try {
            // 这里的myCamera为已经初始化的Camera对象
            if (myCamera != null) {
                myCamera.setPreviewDisplay(myHolder);
            }
        } catch (IOException e) {
            if (callBack != null)
                callBack.callResult("set fail");
            myCamera.stopPreview();
            myCamera.release();
            myCamera = null;
        }
        if (myCamera != null) {
            myCamera.startPreview();
        }
        return true;
    }

    // 自动对焦回调函数(空实现)
    private AutoFocusCallback myAutoFocus = new AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
        }
    };

    // 拍照成功回调函数
    private PictureCallback myPicCallback = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // 完成拍照后关闭Activity
                    finish();
            // 将得到的照片进行270°旋转，使其竖直
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            Matrix matrix = new Matrix();
            matrix.preRotate(270);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);

            // 创建并保存图片文件
            File pictureFile = new File(getDir(), System.currentTimeMillis()
                    + ".jpg");
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception error) {
                if (callBack != null)
                    callBack.callResult("write fail");
                myCamera.stopPreview();
                myCamera.release();
                myCamera = null;
            }
            if (myCamera != null) {
                myCamera.stopPreview();
                myCamera.release();
                myCamera = null;
            }
            if (callBack != null)
                callBack.callResult(pictureFile.getAbsolutePath());
        }

    };

    // 获取文件夹
    private File getDir() {
        // 得到SD卡根目录
        File temp = Environment.getExternalStorageDirectory();
        String dirPaht = temp.toString() + "/OnMinDaPhotos";
        File dir = new File(dirPaht);
        if (dir.exists()) {
            return dir;
        } else {
            dir.mkdirs();
            return dir;
        }
    }

    public interface CallBack {
        void callResult(String result);
    }
}
