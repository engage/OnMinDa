package com.newthread.android.camera;

 public abstract class ServiceState {
	private boolean isaLive;

	public boolean getIsaLive() {
		return isaLive;
	}

	public void setIsaLive(boolean isaLive) {
		this.isaLive = isaLive;
	}
	 
	
}
